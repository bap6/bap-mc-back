import { Controller, Get, Param } from '@nestjs/common';
import { PlayerService } from './player.service';

@Controller('player')
export class PlayerController {
  constructor(private playerService: PlayerService) {}

  @Get('online')
  async getOnlinePlayers() {
    return await this.playerService.getOnlinePlayers();
  }

  @Get(':username/playtime')
  async getPlayTime(@Param() params) {
    return await this.playerService.getPlayTime(params.username);
  }

  @Get(':username/gears')
  async getGears(@Param() params) {
    return await this.playerService.getGears(params.username);
  }
}
