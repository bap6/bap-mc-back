import { Injectable } from '@nestjs/common';
import { ExecuteService } from 'src/execute/execute.service';

@Injectable()
export class PlayerService {
  constructor(private readonly executeService: ExecuteService) {}

  async getOnlinePlayers() {
    return await this.executeService.cmd('list');
  }

  async getPlayTime(username: string) {
    return await this.executeService.cmd(`playtime ${username}`);
  }

  async getGears(username: string) {
    const helmet = await this.executeService.cmd(`data get entity ${username} Inventory[{Slot: 103b}]`);
    // console.log(helmet)
    const chest = await this.executeService.cmd(`data get entity ${username} Inventory[{Slot: 102b}]`);
    // console.log(chest)
    const legging = await this.executeService.cmd(`data get entity ${username} Inventory[{Slot: 101b}]`);
    // console.log(legging)
    const boots = await this.executeService.cmd(`data get entity ${username} Inventory[{Slot: 100b}]`);
    // console.log(boots)
    return {
      helmet: helmet.split('has the following entity data: ')[1],
      chest: chest.split('has the following entity data: ')[1],
      legging: legging.split('has the following entity data: ')[1],
      boots: boots.split('has the following entity data: ')[1],
    };
  }
}
