import { Module } from '@nestjs/common';
import { ExecuteModule } from 'src/execute/execute.module';
import { PlayerController } from './player.controller';
import { PlayerService } from './player.service';

@Module({
  imports: [ExecuteModule],
  providers: [PlayerService],
  controllers: [PlayerController],
})
export class PlayerModule {}
