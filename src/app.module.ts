import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { JobsUsers } from './jobs/jobs-user.entity';
import { JobsModule } from './jobs/jobs.module';
import { JobsJobs } from './jobs/jobs-jobs.entity';
import { JobsJobNames } from './jobs/jobs-job-names.entity';
import { PlayerModule } from './player/player.module';
import { JobsPoints } from './jobs/jobs-points.entity';
import { ExecuteModule } from './execute/execute.module';
import { DenizenModule } from './denizen/denizen.module';
import { DenizenKoiQuest } from './denizen/denizen-koi-quest.entity';
import { SkillsModule } from './skills/skills.module';
import { Skills } from './skills/skills.entity';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'ms1752.gamedata.io',
      port: 3306,
      username: 'ni3717830_1_DB',
      password: 'null',
      database: 'ni3717830_1_DB',
      entities: [
        JobsUsers,
        JobsJobs,
        JobsJobNames,
        JobsPoints,
        DenizenKoiQuest,
        Skills,
      ],
      synchronize: false,
    }),
    JobsModule,
    PlayerModule,
    ExecuteModule,
    DenizenModule,
    SkillsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
