import { Controller, Get, Param } from '@nestjs/common';
import { DenizenService } from './denizen.service';

@Controller('denizen')
export class DenizenController {
  constructor(private readonly denizenService: DenizenService) {}

  @Get(':playerUUID/koiCount')
  getKoiFeedCount(@Param() params) {
    return this.denizenService.getKoiFeedCount(params.playerUUID);
  }
}
