import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DenizenKoiQuest } from './denizen-koi-quest.entity';

@Injectable()
export class DenizenService {
  constructor(
    @InjectRepository(DenizenKoiQuest)
    private denizenKoiQuestRepository: Repository<DenizenKoiQuest>,
  ) {}

  async getKoiFeedCount(player_uuid: string) {
    const resp = await this.denizenKoiQuestRepository.findOneBy({
      player_uuid,
    });
    if (resp && resp.feed_count) {
      return resp.feed_count;
    }
    return 0;
  }
}
