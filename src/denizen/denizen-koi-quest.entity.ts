import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class DenizenKoiQuest {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  player_uuid: string;

  @Column()
  feed_count: number;
}
