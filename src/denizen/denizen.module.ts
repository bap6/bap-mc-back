import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DenizenKoiQuest } from './denizen-koi-quest.entity';
import { DenizenController } from './denizen.controller';
import { DenizenService } from './denizen.service';

@Module({
  imports: [TypeOrmModule.forFeature([DenizenKoiQuest])],
  providers: [DenizenService],
  controllers: [DenizenController],
})
export class DenizenModule {}
