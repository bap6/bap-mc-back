import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Skills } from './skills.entity';

@Injectable()
export class SkillsService {
  constructor(
    @InjectRepository(Skills)
    private skillsRepository: Repository<Skills>,
  ) {}

  async getSkillsByUuid(uuid: string) {
    return await this.skillsRepository.findOneBy({ ID: uuid });
  }
}
