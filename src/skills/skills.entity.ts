import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({
  name: 'SkillData',
})
export class Skills {
  @PrimaryGeneratedColumn()
  ID: string;

  @Column()
  AGILITY_LEVEL: number;

  @Column()
  AGILITY_XP: number;

  @Column()
  ALCHEMY_LEVEL: number;

  @Column()
  ALCHEMY_XP: number;

  @Column()
  ARCHERY_LEVEL: number;

  @Column()
  ARCHERY_XP: number;

  @Column()
  DEFENSE_LEVEL: number;

  @Column()
  DEFENSE_XP: number;

  @Column()
  ENCHANTING_LEVEL: number;

  @Column()
  ENCHANTING_XP: number;

  @Column()
  ENDURANCE_LEVEL: number;

  @Column()
  ENDURANCE_XP: number;

  @Column()
  EXCAVATION_LEVEL: number;

  @Column()
  EXCAVATION_XP: number;

  @Column()
  FARMING_LEVEL: number;

  @Column()
  FARMING_XP: number;

  @Column()
  FIGHTING_LEVEL: number;

  @Column()
  FIGHTING_XP: number;

  @Column()
  FISHING_LEVEL: number;

  @Column()
  FISHING_XP: number;

  @Column()
  FORAGING_LEVEL: number;

  @Column()
  FORAGING_XP: number;

  @Column()
  FORGING_LEVEL: number;

  @Column()
  FORGING_XP: number;

  @Column()
  HEALING_LEVEL: number;

  @Column()
  HEALING_XP: number;

  @Column()
  MINING_LEVEL: number;

  @Column()
  MINING_XP: number;

  @Column()
  SORCERY_LEVEL: number;

  @Column()
  SORCERY_XP: number;

  @Column()
  LOCALE: number;

  @Column()
  STAT_MODIFIERS: number;

  @Column()
  MANA: number;

  @Column()
  ABILITY_DATA: number;

  @Column()
  UNCLAIMED_ITEMS: number;
}
