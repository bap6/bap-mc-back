import { Controller, Get, Param } from '@nestjs/common';
import { SkillsService } from './skills.service';

@Controller('skills')
export class SkillsController {
  constructor(private readonly skillsService: SkillsService) {}

  @Get(':uuid')
  getSkillsByUuid(@Param() params) {
    return this.skillsService.getSkillsByUuid(params.uuid);
  }
}
