import { Injectable } from '@nestjs/common';
import RCON from 'rcon-srcds';

@Injectable()
export class ExecuteService {
  async cmd(command: string) {
    const rconClient = new RCON({
      host: process.env.RCON_HOST,
      port: parseInt(process.env.RCON_PORT || ''),
      timeout: 5000,
      encoding: 'utf8',
      maxPacketSize: 100000,
    });

    const random = Math.floor(Math.random() * (2 - 1 + 1) + 1);
    await new Promise((resolve) => setTimeout(resolve, random));

    try {
      await rconClient.authenticate(process.env.RCON_PASSWORD || '');
      const response = await rconClient.execute(command);
      await rconClient.disconnect();
      return response.toString();
    } catch (error) {
      console.error(error);
      return null;
    }
  }
}
