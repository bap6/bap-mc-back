import { Module } from '@nestjs/common';
import { ExecuteService } from './execute.service';

@Module({
  imports: [],
  providers: [ExecuteService],
  exports: [ExecuteService],
})
export class ExecuteModule {}
