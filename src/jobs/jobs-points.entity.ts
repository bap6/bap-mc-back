import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class JobsPoints {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  userid: number;

  @Column()
  totalpoints: number;

  @Column()
  currentpoints: number;
}
