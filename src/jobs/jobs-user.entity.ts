import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class JobsUsers {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  player_uuid: string;

  @Column()
  username: string;

  @Column()
  seen: number;

  @Column()
  donequests: number;

  @Column()
  quests: string;
}
