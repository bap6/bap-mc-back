import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SharpService } from 'nestjs-sharp';
import { ExecuteService } from 'src/execute/execute.service';
import { Repository } from 'typeorm';
import { JobsJobNames } from './jobs-job-names.entity';
import { JobsJobs } from './jobs-jobs.entity';
import { JobsPoints } from './jobs-points.entity';
import { JobsUsers } from './jobs-user.entity';

@Injectable()
export class JobsService {
  constructor(
    @InjectRepository(JobsUsers)
    private jobsUserRepository: Repository<JobsUsers>,
    @InjectRepository(JobsJobs)
    private jobsJobsRepository: Repository<JobsJobs>,
    @InjectRepository(JobsJobNames)
    private jobsJobNamesRepository: Repository<JobsJobNames>,
    @InjectRepository(JobsPoints)
    private jobsJobsPoints: Repository<JobsPoints>,
    private readonly httpService: HttpService,
    private readonly sharpService: SharpService,
    private readonly executeService: ExecuteService,
  ) {}

  async getJobsList() {
    return await this.jobsJobNamesRepository.find();
  }

  async getJobsUsers() {
    let users: Partial<JobsUsers>[] = await this.jobsUserRepository.find();

    users = await Promise.all(
      users.map(async (user) => {
        try {
          const player = await this.httpService
            .get(
              `
                https://api.mojang.com/users/profiles/minecraft/${user.username}
              `,
            )
            .toPromise();
          const avatar = await this.httpService
            .get(
              `
                https://sessionserver.mojang.com/session/minecraft/profile/${player.data.id}
              `,
            )
            .toPromise();

          const image = await this.httpService
            .get(
              JSON.parse(
                new Buffer(avatar.data.properties[0].value, 'base64').toString(
                  'ascii',
                ),
              ).textures.SKIN.url,
              {
                responseType: 'arraybuffer',
              },
            )
            .toPromise();

          const skin = JSON.parse(
            new Buffer(avatar.data.properties[0].value, 'base64').toString(
              'ascii',
            ),
          ).textures.SKIN.url;

          const backBuffer = await this.sharpService
            .edit(image.data)
            .extract({
              left: 8,
              top: 8,
              width: 8,
              height: 8,
            })
            .toBuffer();
          const frontBuffer = await this.sharpService
            .edit(image.data)
            .extract({
              left: 40,
              top: 8,
              width: 8,
              height: 8,
            })
            .toBuffer();
          const finalBuffer = await this.sharpService
            .edit(backBuffer)
            .composite([{ input: frontBuffer, gravity: 'southeast' }])
            .sharpen()
            .toBuffer();

          return {
            ...user,
            avatar: `data:image/png;base64,${finalBuffer.toString('base64')}`,
            skin,
          };
        } catch (error) {
          throw error;
        }
      }),
    );
    return users;
  }

  async getJobsForUser(userUUID: string) {
    const user = await this.jobsUserRepository.findOneBy({
      player_uuid: userUUID,
    });
    return await this.jobsJobsRepository.findBy({ userid: user.id });
  }

  async getJobsPoints(userUUID: string) {
    const user = await this.jobsUserRepository.findOneBy({
      player_uuid: userUUID,
    });
    return await this.jobsJobsPoints.findOneBy({ userid: user.id });
  }
}
