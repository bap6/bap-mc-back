import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class JobsJobs {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  userid: number;

  @Column()
  job: string;

  @Column()
  experience: number;

  @Column()
  level: number;

  @Column()
  jobid: number;
}
