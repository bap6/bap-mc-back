import { Controller, Get, Param } from '@nestjs/common';
import { JobsService } from './jobs.service';

@Controller('jobs')
export class JobsController {
  constructor(private readonly jobsService: JobsService) {}

  @Get('list')
  getJobsList() {
    return this.jobsService.getJobsList();
  }

  @Get('players')
  getJobsPlayers() {
    return this.jobsService.getJobsUsers();
  }

  @Get(':playerUUID/points')
  getJobsPoints(@Param() params) {
    return this.jobsService.getJobsPoints(params.playerUUID);
  }

  @Get(':playerUUID')
  getJobsForPlayer(@Param() params) {
    return this.jobsService.getJobsForUser(params.playerUUID);
  }
}
