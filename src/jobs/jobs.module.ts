import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharpModule } from 'nestjs-sharp';
import { JobsUsers } from './jobs-user.entity';
import { JobsJobs } from './jobs-jobs.entity';
import { JobsController } from './jobs.controller';
import { JobsService } from './jobs.service';
import { JobsJobNames } from './jobs-job-names.entity';
import { ExecuteModule } from 'src/execute/execute.module';
import { JobsPoints } from './jobs-points.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([JobsJobs, JobsUsers, JobsJobNames, JobsPoints]),
    HttpModule,
    SharpModule,
    ExecuteModule,
  ],
  providers: [JobsService],
  controllers: [JobsController],
})
export class JobsModule {}
