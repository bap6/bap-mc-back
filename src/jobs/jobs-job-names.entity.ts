import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({
  name: 'jobs_jobNames',
})
export class JobsJobNames {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
}
