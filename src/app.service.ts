import { Injectable } from '@nestjs/common';
import { ExecuteService } from './execute/execute.service';

@Injectable()
export class AppService {
  constructor(private executeService: ExecuteService) {}

  async getHello() {
    const resp = await this.executeService.cmd('lag');
    return {
      uptime: resp.split('§6Uptime:§c ')[1].split('\n')[0],
      tps: parseInt(resp.split('§6Current TPS = §a')[1].split('\n')[0]),
      maxMem: parseInt(
        resp
          .split('§6Maximum memory:§c ')[1]
          .split(' MB.\n')[0]
          .replace(',', ''),
      ),
      allocMem: parseInt(
        resp
          .split('§6Allocated memory:§c ')[1]
          .split(' MB.\n')[0]
          .replace(',', ''),
      ),
      freeMem: parseInt(
        resp.split('§6Free memory:§c ')[1].split(' MB.\n')[0].replace(',', ''),
      ),
    };
  }
}
